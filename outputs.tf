output "instance_uuids" {
  value = concat([openstack_compute_instance_v2.os_master_instance.0.id], tolist(openstack_compute_instance_v2.os_worker_instance.*.id))
}
output "master_floatingip" {
  value = var.master_floating_ip == "" ? openstack_networking_floatingip_v2.os_master_floatingip.0.address : var.master_floating_ip
}